﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class b_Bouche : MonoBehaviour
{
    public GameObject Bouche;

    public List<GameObject> sousBouche;
    public Sprite[] visuBouche;

    public ColorPicker picker;
    public Color Color = Color.white;

    public string[] textBouche;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < 6; i++)
        {
            sousBouche.Add(GameObject.Find("b_Bouche" + i.ToString()));
            sousBouche[i - 1].SetActive(false);
        }

        Bouche = GameObject.Find("Bouche");

        Button btn1 = sousBouche[0].GetComponent<Button>();
        btn1.onClick.AddListener(SousBouche1);
        Button btn2 = sousBouche[1].GetComponent<Button>();
        btn2.onClick.AddListener(SousBouche2);
        Button btn3 = sousBouche[2].GetComponent<Button>();
        btn3.onClick.AddListener(SousBouche3);
        Button btn4 = sousBouche[3].GetComponent<Button>();
        btn4.onClick.AddListener(SousBouche4);
        Button btn5 = sousBouche[4].GetComponent<Button>();
        btn5.onClick.AddListener(SousBouche5);

        picker.onValueChanged.AddListener(color =>
        {
            Bouche.GetComponent<Image>().color = color;
            Color = color;
        });

        Bouche.GetComponent<Image>().color = picker.CurrentColor;

        picker.CurrentColor = Color;

        Zrandom();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<ButtonManager>().bp == 5)
        {
            gameObject.GetComponent<Image>().color = Color.white;
            for (int i = 1; i < 6; i++)
            {
                sousBouche[i - 1].SetActive(true);
            }
        }
        else
        {
            gameObject.GetComponent<Image>().color = Color.grey;
            for (int i = 1; i < 6; i++)
            {
                sousBouche[i - 1].SetActive(false);
            }
        }
    }

    public void SousBouche1()
    {
        Bouche.GetComponent<Image>().sprite = visuBouche[0];
        GeneName();
    }

    public void SousBouche2()
    {
        Bouche.GetComponent<Image>().sprite = visuBouche[1];
        GeneName();
    }

    public void SousBouche3()
    {
        Bouche.GetComponent<Image>().sprite = visuBouche[2];
        GeneName();
    }

    public void SousBouche4()
    {
        Bouche.GetComponent<Image>().sprite = visuBouche[3];
        GeneName();
    }

    public void SousBouche5()
    {
        Bouche.GetComponent<Image>().sprite = visuBouche[4];
        GeneName();
    }

    public void Zrandom()
    {
        Bouche.GetComponent<Image>().sprite = visuBouche[Random.Range(0, visuBouche.Length)];
        GeneName();
    }

    public void GeneName()
    {
        GetComponentInParent<ButtonManager>().t_Bouche.text = textBouche[Random.Range(0, textBouche.Length - 1)];
    }
}