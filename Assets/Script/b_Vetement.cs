﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class b_Vetement : MonoBehaviour
{
    public GameObject Vetement;

    public List<GameObject> sousVetement;
    public Sprite[] visuVetement;

    public ColorPicker picker;
    public Color Color = Color.white;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < 7; i++)
        {
            sousVetement.Add(GameObject.Find("b_Vetement" + i.ToString()));
            sousVetement[i - 1].SetActive(false);
        }

        Vetement = GameObject.Find("Vetement");

        Button btn1 = sousVetement[0].GetComponent<Button>();
        btn1.onClick.AddListener(SousVetement1);
        Button btn2 = sousVetement[1].GetComponent<Button>();
        btn2.onClick.AddListener(SousVetement2);
        Button btn3 = sousVetement[2].GetComponent<Button>();
        btn3.onClick.AddListener(SousVetement3);
        Button btn4 = sousVetement[3].GetComponent<Button>();
        btn4.onClick.AddListener(SousVetement4);
        Button btn5 = sousVetement[4].GetComponent<Button>();
        btn5.onClick.AddListener(SousVetement5);
        Button btn6 = sousVetement[5].GetComponent<Button>();
        btn6.onClick.AddListener(SousVetement6);

        picker.onValueChanged.AddListener(color =>
        {
            Vetement.GetComponent<Image>().color = color;
            Color = color;
        });

        Vetement.GetComponent<Image>().color = picker.CurrentColor;

        picker.CurrentColor = Color;

        Zrandom();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<ButtonManager>().bp == 6)
        {
            for (int i = 1; i < 7; i++)
            {
                sousVetement[i - 1].SetActive(true);
            }
        }
        else
        {
            for (int i = 1; i < 7; i++)
            {
                sousVetement[i - 1].SetActive(false);
            }
        }
    }

    public void SousVetement1()
    {
        Vetement.GetComponent<Image>().sprite = visuVetement[0];
    }

    public void SousVetement2()
    {
        Vetement.GetComponent<Image>().sprite = visuVetement[1];
    }

    public void SousVetement3()
    {
        Vetement.GetComponent<Image>().sprite = visuVetement[2];
    }

    public void SousVetement4()
    {
        Vetement.GetComponent<Image>().sprite = visuVetement[3];
    }

    public void SousVetement5()
    {
        Vetement.GetComponent<Image>().sprite = visuVetement[4];
    }

    public void SousVetement6()
    {
        Vetement.GetComponent<Image>().sprite = visuVetement[5];
    }

    public void Zrandom()
    {
        //Vetement.GetComponent<Image>().sprite = visuVetement[Random.Range(0, visuVetement.Length)];
    }
}
