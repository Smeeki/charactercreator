﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieName : MonoBehaviour
{

    public string LettreCorps;
    public string LettreTete;
    public string LettreNez;
    public string LettreBouche;
    public string LettreYeux;

    void Start()
    {
        LettreCorps = "";
        LettreTete = "";
        LettreNez = "";
        LettreBouche = "";
        LettreYeux = "";
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            Debug.Log(LettreCorps + LettreTete + LettreNez + LettreBouche + LettreYeux);
        }
    }
}
