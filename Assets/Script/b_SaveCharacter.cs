﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class b_SaveCharacter : MonoBehaviour
{
    private static b_SaveCharacter instance;
    public Camera MyCamera;
    private bool TakeScreenShotOnNextFrame;

    public GameObject UiInterface;

    public AudioSource audioData;

    public GameObject b_Manage;
    public ButtonManager BM;

    void Start()
    {
        audioData = GetComponent<AudioSource>();

        b_Manage = GameObject.Find("Canvas");
        BM = b_Manage.GetComponent<ButtonManager>();
    }

    private void Awake()
    {
        instance = this;
        MyCamera = gameObject.GetComponent<Camera>();
    }

    public void TakeScreenShot()
    {
        UiInterface.SetActive(false);
        TakeScreenShot_Static(1920, 1080);
        audioData.Play();
    }

    private void OnPostRender()
    {
        if(TakeScreenShotOnNextFrame == true)
        {
            TakeScreenShotOnNextFrame = false;
            RenderTexture renderTexture = MyCamera.targetTexture;

            Texture2D renderResult = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
            Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
            renderResult.ReadPixels(rect, 0, 0);

            byte[] byteArray = renderResult.EncodeToJPG();
            System.IO.File.WriteAllBytes(Application.dataPath + "/SavedCharacter/Z" + BM.t_Corps.text + BM.t_Tete.text + BM.t_Yeux.text + BM.t_Nez.text + BM.t_Bouche.text + ".jpg", byteArray);
            //Debug.Log("Saved CameraScreenShot.jpg");
            RenderTexture.ReleaseTemporary(renderTexture);
            MyCamera.targetTexture = null;
            UiInterface.SetActive(true);
        }
    }

    private void TakeScreenShot(int width, int height)
    {
        MyCamera.targetTexture = RenderTexture.GetTemporary(width, height, 16);
        TakeScreenShotOnNextFrame = true;
    }

    public static void TakeScreenShot_Static(int width, int height)
    {
        instance.TakeScreenShot(width, height);
    }


}
