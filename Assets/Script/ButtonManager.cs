﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public Button b_Corps, b_Tete, b_Yeux, b_Nez, b_Bouche, b_Vetement, b_Random;
    public GameObject b_Color;
    public int bp;

    public Text t_Corps, t_Tete, t_Yeux, t_Nez, t_Bouche;

    public AudioSource audioDataRandom;

    // Start is called before the first frame update
    void Start()
    {
        b_Corps.onClick.AddListener(Corps);
        b_Tete.onClick.AddListener(Tete);
        b_Yeux.onClick.AddListener(Yeux);
        b_Nez.onClick.AddListener(Nez);
        b_Bouche.onClick.AddListener(Bouche);
        b_Vetement.onClick.AddListener(Vetement);
        b_Random.onClick.AddListener(ZombieRandom);
        b_Color.SetActive(false);
        bp = 0;

        audioDataRandom = b_Random.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Corps()
    {
        if(bp == 1)
        {
            bp = 0;
            b_Color.GetComponent<b_Color>().Desactive();
            b_Color.SetActive(false);
        }
        else
        {
            bp = 1;
            b_Corps.GetComponent<b_Corps>().audioData.Play();
            b_Color.SetActive(true);
        }
    }

    public void Tete()
    {
        if (bp == 2)
        {
            bp = 0;
            b_Color.GetComponent<b_Color>().Desactive();
            b_Color.SetActive(false);
        }
        else
        {
            bp = 2;
            b_Corps.GetComponent<b_Corps>().audioData.Play();
            b_Color.SetActive(true);
        }
    }

    public void Yeux()
    {
        if (bp == 3)
        {
            bp = 0;
            b_Color.GetComponent<b_Color>().Desactive();
            b_Color.SetActive(false);
        }
        else
        {
            bp = 3;
            b_Corps.GetComponent<b_Corps>().audioData.Play();
            b_Color.SetActive(true);
        }
    }

    public void Nez()
    {
        if (bp == 4)
        {
            bp = 0;
            b_Color.GetComponent<b_Color>().Desactive();
            b_Color.SetActive(false);
        }
        else
        {
            bp = 4;
            b_Corps.GetComponent<b_Corps>().audioData.Play();
            b_Color.SetActive(true);
        }
    }

    public void Bouche()
    {
        if (bp == 5)
        {
            bp = 0;
            b_Color.GetComponent<b_Color>().Desactive();
            b_Color.SetActive(false);
        }
        else
        {
            bp = 5;
            b_Corps.GetComponent<b_Corps>().audioData.Play();
            b_Color.SetActive(true);
        }
    }

    public void Vetement()
    {
        if (bp == 6)
        {
            bp = 0;
            b_Color.GetComponent<b_Color>().Desactive();
            b_Color.SetActive(false);
        }
        else
        {
            bp = 6;
            b_Corps.GetComponent<b_Corps>().audioData.Play();
            b_Color.SetActive(true);
        }
    }

    public void ZombieRandom()
    {
        //Debug.Log("start");
        audioDataRandom.Play();
        b_Corps.GetComponent<b_Corps>().Zrandom();
        b_Tete.GetComponent<b_Tete>().Zrandom();
        b_Yeux.GetComponent<b_Yeux>().Zrandom();
        b_Nez.GetComponent<b_Nez>().Zrandom();
        b_Bouche.GetComponent<b_Bouche>().Zrandom();
        b_Vetement.GetComponent<b_Vetement>().Zrandom();
    }
}
