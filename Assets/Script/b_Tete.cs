﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class b_Tete : MonoBehaviour
{
    public GameObject Tete;

    public List<GameObject> sousTete;
    public Sprite[] visuTete;

    public ColorPicker picker;
    public Color Color = Color.white;

    public string[] textTete;

    public AudioSource audioData;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < 6; i++)
        {
            sousTete.Add(GameObject.Find("b_Tete" + i.ToString()));
            sousTete[i - 1].SetActive(false);
        }

        Tete = GameObject.Find("Tete");

        Button btn1 = sousTete[0].GetComponent<Button>();
        btn1.onClick.AddListener(SousTete1);
        Button btn2 = sousTete[1].GetComponent<Button>();
        btn2.onClick.AddListener(SousTete2);
        Button btn3 = sousTete[2].GetComponent<Button>();
        btn3.onClick.AddListener(SousTete3);
        Button btn4 = sousTete[3].GetComponent<Button>();
        btn4.onClick.AddListener(SousTete4);
        Button btn5 = sousTete[4].GetComponent<Button>();
        btn5.onClick.AddListener(SousTete5);

        picker.onValueChanged.AddListener(color =>
        {
            Tete.GetComponent<Image>().color = color;
            Color = color;
        });

        Tete.GetComponent<Image>().color = picker.CurrentColor;

        picker.CurrentColor = Color;

        Zrandom();

        audioData = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<ButtonManager>().bp == 2)
        {
            gameObject.GetComponent<Image>().color = Color.white;
            for (int i = 1; i < 6; i++)
            {
                sousTete[i - 1].SetActive(true);
            }
        }
        else
        {
            gameObject.GetComponent<Image>().color = Color.grey;
            for (int i = 1; i < 6; i++)
            {
                sousTete[i - 1].SetActive(false);
            }
        }
    }

    public void SousTete1()
    {
        Tete.GetComponent<Image>().sprite = visuTete[0];
        GeneName();
    }

    public void SousTete2()
    {
        Tete.GetComponent<Image>().sprite = visuTete[1];
        GeneName();
    }

    public void SousTete3()
    {
        Tete.GetComponent<Image>().sprite = visuTete[2];
        GeneName();
    }

    public void SousTete4()
    {
        Tete.GetComponent<Image>().sprite = visuTete[3];
        GeneName();
    }

    public void SousTete5()
    {
        Tete.GetComponent<Image>().sprite = visuTete[4];
        GeneName();
    }

    public void Zrandom()
    {
        Tete.GetComponent<Image>().sprite = visuTete[Random.Range(0, visuTete.Length)];
        GeneName();
    }

    public void GeneName()
    {
        GetComponentInParent<ButtonManager>().t_Tete.text = textTete[Random.Range(0, textTete.Length - 1)];
    }
}
