﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class FileSearch : MonoBehaviour
{
    

    // Start is called before the first frame update
    void Start()
    {
        DirectoryInfo dir = new DirectoryInfo("Head"); //Chercher la direction du fichier
        if (!dir.Exists) //S'il n'existe pas
        {
            dir.Create(); //Il le créer
        }

        FileInfo[] info = dir.GetFiles("*.png"); //Tu mets dans le tableau le chemin de tous les fichier .png

        foreach (FileInfo f in info) //Pour chaque chemin tu me fais ça
        {
            Debug.Log(f);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
