﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class b_Share : MonoBehaviour
{
    public GameObject reseau;

    // Start is called before the first frame update
    void Start()
    {
        Ferme();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonReseaux()
    {
        reseau.SetActive(true);
    }

    public void OuvreTwitter()
    {
        Application.OpenURL("https://twitter.com/");
        Ferme();
    }

    public void OuvreFaceBook()
    {
        Application.OpenURL("https://www.facebook.com/");
        Ferme();
    }

    public void Ferme()
    {
        reseau.SetActive(false);
    }
}
