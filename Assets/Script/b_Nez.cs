﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class b_Nez : MonoBehaviour
{
    public GameObject Nez;


    public List<GameObject> sousNez;
    public Sprite[] visuNez;

    public ColorPicker picker;
    public Color Color = Color.white;

    public string[] textNez;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < 6; i++)
        {
            sousNez.Add(GameObject.Find("b_Nez" + i.ToString()));
            sousNez[i - 1].SetActive(false);
        }

        Nez = GameObject.Find("Nez");

        Button btn1 = sousNez[0].GetComponent<Button>();
        btn1.onClick.AddListener(SousNez1);
        Button btn2 = sousNez[1].GetComponent<Button>();
        btn2.onClick.AddListener(SousNez2);
        Button btn3 = sousNez[2].GetComponent<Button>();
        btn3.onClick.AddListener(SousNez3);
        Button btn4 = sousNez[3].GetComponent<Button>();
        btn4.onClick.AddListener(SousNez4);
        Button btn5 = sousNez[4].GetComponent<Button>();
        btn5.onClick.AddListener(SousNez5);

        picker.onValueChanged.AddListener(color =>
        {
            Nez.GetComponent<Image>().color = color;
            Color = color;
        });

        Nez.GetComponent<Image>().color = picker.CurrentColor;

        picker.CurrentColor = Color;

        Zrandom();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<ButtonManager>().bp == 4)
        {
            gameObject.GetComponent<Image>().color = Color.white;
            for (int i = 1; i < 6; i++)
            {
                sousNez[i - 1].SetActive(true);
            }
        }
        else
        {
            gameObject.GetComponent<Image>().color = Color.grey;
            for (int i = 1; i < 6; i++)
            {
                sousNez[i - 1].SetActive(false);
            }
        }
    }

    public void SousNez1()
    {
        Nez.GetComponent<Image>().sprite = visuNez[0];
        GeneName();
    }

    public void SousNez2()
    {
        Nez.GetComponent<Image>().sprite = visuNez[1];
        GeneName();
    }

    public void SousNez3()
    {
        Nez.GetComponent<Image>().sprite = visuNez[2];
        GeneName();
    }

    public void SousNez4()
    {
        Nez.GetComponent<Image>().sprite = visuNez[3];
        GeneName();
    }

    public void SousNez5()
    {
        Nez.GetComponent<Image>().sprite = visuNez[4];
        GeneName();
    }

    public void Zrandom()
    {
        Nez.GetComponent<Image>().sprite = visuNez[Random.Range(0, visuNez.Length)];
        GeneName();
    }

    public void GeneName()
    {
        GetComponentInParent<ButtonManager>().t_Nez.text = textNez[Random.Range(0, textNez.Length - 1)];
    }
}
