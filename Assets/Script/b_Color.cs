﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class b_Color : MonoBehaviour
{

    public GameObject[] Picker;
    public GameObject b_Tete;
    bool push;
    // Start is called before the first frame update
    void Start()
    {
        Button btn1 = gameObject.GetComponent<Button>();
        btn1.onClick.AddListener(TaskOnClick);

        push = false;

        for (int i = 0; i < 6; i++)
        {
            Picker[i].SetActive(false);
        }

        b_Tete = GameObject.Find("b_Tete");
    }

    // Update is called once per frame
    void Update()
    {
        if (push)
        {
            if (GetComponentInParent<ButtonManager>().bp == 1)
            {
                for (int i = 0; i < 6; i++)
                {
                    Picker[i].SetActive(false);
                }
                Picker[0].SetActive(true);
            }
            if (GetComponentInParent<ButtonManager>().bp == 2)
            {
                for (int i = 0; i < 6; i++)
                {
                    Picker[i].SetActive(false);
                }
                Picker[1].SetActive(true);
            }
            if (GetComponentInParent<ButtonManager>().bp == 3)
            {
                for (int i = 0; i < 6; i++)
                {
                    Picker[i].SetActive(false);
                }
                Picker[2].SetActive(true);
            }
            if (GetComponentInParent<ButtonManager>().bp == 4)
            {
                for (int i = 0; i < 6; i++)
                {
                    Picker[i].SetActive(false);
                }
                Picker[3].SetActive(true);
            }
            if (GetComponentInParent<ButtonManager>().bp == 5)
            {
                for (int i = 0; i < 6; i++)
                {
                    Picker[i].SetActive(false);
                }
                Picker[4].SetActive(true);
            }
            if (GetComponentInParent<ButtonManager>().bp == 6)
            {
                for (int i = 0; i < 6; i++)
                {
                    Picker[i].SetActive(false);
                }
                Picker[5].SetActive(true);
            }
        }
        else
        {
            Desactive();
        }

    }

    public void TaskOnClick()
    {
        if (push)
        {
            //Debug.Log("false");
            push = false;
        }
        else
        {
            b_Tete.GetComponent<b_Tete>().audioData.Play();

            if (push == false)
            {
                //Debug.Log("true");
                push = true;
            }
        }
    }

    public void Desactive()
    {
        for (int i = 0; i < 6; i++)
        {
            Picker[i].SetActive(false);
        }
    }
}
