﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class b_Yeux : MonoBehaviour
{
    public GameObject Yeux;

    public List<GameObject> sousYeux;
    public Sprite[] visuYeux;

    public ColorPicker picker;
    public Color Color = Color.white;

    public string[] textYeux;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < 6; i++)
        {
            sousYeux.Add(GameObject.Find("b_Yeux" + i.ToString()));
            sousYeux[i - 1].SetActive(false);
        }

        Yeux = GameObject.Find("Yeux");

        Button btn1 = sousYeux[0].GetComponent<Button>();
        btn1.onClick.AddListener(SousYeux1);
        Button btn2 = sousYeux[1].GetComponent<Button>();
        btn2.onClick.AddListener(SousYeux2);
        Button btn3 = sousYeux[2].GetComponent<Button>();
        btn3.onClick.AddListener(SousYeux3);
        Button btn4 = sousYeux[3].GetComponent<Button>();
        btn4.onClick.AddListener(SousYeux4);
        Button btn5 = sousYeux[4].GetComponent<Button>();
        btn5.onClick.AddListener(SousYeux5);

        picker.onValueChanged.AddListener(color =>
        {
            Yeux.GetComponent<Image>().color = color;
            Color = color;
        });

        Yeux.GetComponent<Image>().color = picker.CurrentColor;

        picker.CurrentColor = Color;

        Zrandom();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<ButtonManager>().bp == 3)
        {
            gameObject.GetComponent<Image>().color = Color.white;
            for (int i = 1; i < 6; i++)
            {
                sousYeux[i - 1].SetActive(true);
            }
        }
        else
        {
            gameObject.GetComponent<Image>().color = Color.grey;
            for (int i = 1; i < 6; i++)
            {
                sousYeux[i - 1].SetActive(false);
            }
        }
    }

    public void SousYeux1()
    {
        Yeux.GetComponent<Image>().sprite = visuYeux[0];
        GeneName();
    }

    public void SousYeux2()
    {
        Yeux.GetComponent<Image>().sprite = visuYeux[1];
        GeneName();
    }

    public void SousYeux3()
    {
        Yeux.GetComponent<Image>().sprite = visuYeux[2];
        GeneName();
    }

    public void SousYeux4()
    {
        Yeux.GetComponent<Image>().sprite = visuYeux[3];
        GeneName();
    }

    public void SousYeux5()
    {
        Yeux.GetComponent<Image>().sprite = visuYeux[4];
        GeneName();
    }

    public void Zrandom()
    {
        Yeux.GetComponent<Image>().sprite = visuYeux[Random.Range(0, visuYeux.Length)];
        GeneName();
    }

    public void GeneName()
    {
        GetComponentInParent<ButtonManager>().t_Yeux.text = textYeux[Random.Range(0, textYeux.Length - 1)];
    }
}
