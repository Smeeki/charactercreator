﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class SearchBouche : MonoBehaviour
{
    public Button b_Refresh;

    // Start is called before the first frame update
    void Start()
    {
        DirectoryInfo dir = new DirectoryInfo("Image/Bouche"); //Chercher la direction du fichier
        if (!dir.Exists) //S'il n'existe pas
        {
            dir.Create(); //Il le créer
        }

        FileInfo[] info = dir.GetFiles("*.png"); //Tu mets dans le tableau le chemin de tous les fichier .png

        foreach (FileInfo f in info) //Pour chaque chemin tu me fais ça
        {
            Debug.Log(f);
        }

        Button btn1 = b_Refresh.GetComponent<Button>();
        btn1.onClick.AddListener(Refresh); //si le bouton est cliqué, jouer boucle Refresh
    }

        // Update is called once per frame
        void Update()
    {
        
    }

    public void Refresh()
    {
        DirectoryInfo dir = new DirectoryInfo("Image/Bouche"); //Chercher la direction du fichier
        if (!dir.Exists) //S'il n'existe pas
        {
            dir.Create(); //Il le créer
        }

        FileInfo[] info = dir.GetFiles("*.png"); //Tu mets dans le tableau le chemin de tous les fichier .png

        foreach (FileInfo f in info) //Pour chaque chemin tu me fais ça
        {
            Debug.Log(f);
        }
    }
}
