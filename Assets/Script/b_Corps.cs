﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class b_Corps : MonoBehaviour
{
    public GameObject Corps;

    public List<GameObject> sousCorps;
    public Sprite[] visuCorps;

    public ColorPicker picker;
    public Color Color = Color.white;

    public string[] textCorps;

    public AudioSource audioData;

    // Start is called before the first frame update
    void Start()
    {
        

        for (int i = 1; i < 6; i++)
        {
            sousCorps.Add(GameObject.Find("b_Corps" + i.ToString()));
            sousCorps[i - 1].SetActive(false);
        }

        Corps = GameObject.Find("Corps");

        Button btn1 = sousCorps[0].GetComponent<Button>();
        btn1.onClick.AddListener(SousCorps1);
        Button btn2 = sousCorps[1].GetComponent<Button>();
        btn2.onClick.AddListener(SousCorps2);
        Button btn3 = sousCorps[2].GetComponent<Button>();
        btn3.onClick.AddListener(SousCorps3);
        Button btn4 = sousCorps[3].GetComponent<Button>();
        btn4.onClick.AddListener(SousCorps4);
        Button btn5 = sousCorps[4].GetComponent<Button>();
        btn5.onClick.AddListener(SousCorps5);

        
        picker.onValueChanged.AddListener(color =>
        {
            Corps.GetComponent<Image>().color = color;
            Color = color;
        });

        Corps.GetComponent<Image>().color = picker.CurrentColor;

        picker.CurrentColor = Color;

        Zrandom();

        audioData = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<ButtonManager>().bp == 1)
        {
            gameObject.GetComponent<Image>().color = Color.white;
            for (int i = 1; i < 6; i++)
            {
                sousCorps[i - 1].SetActive(true);
            }
        }
        else
        {
            gameObject.GetComponent<Image>().color = Color.grey;
            for (int i = 1; i < 6; i++)
            {
                sousCorps[i - 1].SetActive(false);
            }
            
        }
    }

    public void SousCorps1()
    {
        Corps.GetComponent<Image>().sprite = visuCorps[0];
        GeneName();
    }

    public void SousCorps2()
    {
        Corps.GetComponent<Image>().sprite = visuCorps[1];
        GeneName();
    }

    public void SousCorps3()
    {
        Corps.GetComponent<Image>().sprite = visuCorps[2];
        GeneName();
    }

    public void SousCorps4()
    {
        Corps.GetComponent<Image>().sprite = visuCorps[3];
        GeneName();
    }

    public void SousCorps5()
    {
        Corps.GetComponent<Image>().sprite = visuCorps[4];
        GeneName();
    }

    public void Zrandom()
    {
        Corps.GetComponent<Image>().sprite = visuCorps[Random.Range(0, visuCorps.Length)];
        GeneName();
    }

    public void GeneName()
    {
        GetComponentInParent<ButtonManager>().t_Corps.text = textCorps[Random.Range(0, textCorps.Length - 1)];
    }
}
